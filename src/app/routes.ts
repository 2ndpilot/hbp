import { Routes } from '@angular/router';
import { NotFoundPageComponent } from './core/containers/not-found-page.component';

export const routes: Routes = [
  { path: '', redirectTo: '/breaches', pathMatch: 'full' },
  { path: 'breaches', loadChildren: 'app/breaches/breaches.module#BreachesModule' },
  { path: '**', component: NotFoundPageComponent },
];
