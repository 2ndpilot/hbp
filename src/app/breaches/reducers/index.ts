import * as fromRoot from '../../reducers';
import * as fromSearch from './search.reducer';
import { Breach } from '../models/breach.model';
import { BreachActions, BreachActionTypes } from '../actions/breach.actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createSelector, createFeatureSelector } from '@ngrx/store';
import { Option } from '../models/option.model';
import { filterMatch } from '../../shared/utils';

export interface BreachesState extends EntityState<Breach> {
  loading: boolean;
  filter: string;
  filtredOptions: Option[];
  selectedOption: Option;
}

export interface State extends fromRoot.State {
  breaches: BreachesState;
  search: fromSearch.State;
}

export const adapter: EntityAdapter<Breach> = createEntityAdapter<Breach>({
  selectId: (breach: Breach) => breach.Title,
  sortComparer: false,
});

export const getBreachState = createFeatureSelector<State>('breaches');
export const getBreachEntitiesState = createSelector(
  getBreachState,
  state => state.breaches
);

export const {
  selectIds: getBreachIds,
  selectEntities: getBreachEntities,
  selectAll: getAllBreaches,
  selectTotal: getTotalBreachs,
} = adapter.getSelectors(getBreachEntitiesState);

export const getBreachesLoading = createSelector(
  getBreachEntitiesState,
  state => state.loading
);

export const getAutocompleteOptions = createSelector(
  getBreachEntitiesState,
  state => state.filtredOptions
);

export const getBreachesFilter = createSelector(
  getBreachEntitiesState,
  state => state.filter
);

export const getSearchState = createSelector(
  getBreachState,
  state => state.search
);

export const getSearchDomain = createSelector(
  getSearchState,
  state => state.domain
);

export const getSearchQuery = createSelector(
  getSearchState,
  state => state.query
);

export const getSearchLoading = createSelector(
  getSearchState,
  state => state.loading
);

export const getSearchError = createSelector(
  getSearchState,
  state => state.error
);

export const getSearchResults = createSelector(
  getSearchState,
  state => state.result
);

export const initialState: BreachesState = adapter.getInitialState({
  loading: false,
  filter: '',
  filtredOptions: [],
  selectedOption: null
});

export function reducer(
  state = initialState,
  action: BreachActions
): BreachesState {
  switch (action.type) {
    case BreachActionTypes.LoadAll: {
      return {
        ...state,
        loading: true
      };
    }

    case BreachActionTypes.LoadAllSuccess: {
      return {
        ...adapter.addAll(action.payload, state),
        loading: false
      };
    }

    case BreachActionTypes.FilterBreaches: {
      const filter = action.payload;
      const options: Option[] = [];

      if (filter === state.filter) {
        return {
          ...state
        };
      }

      adapter.getSelectors().selectAll(state).forEach((breach: Breach) => {
        if (filterMatch(filter, breach)) {
          options.push({name: breach.Name});
        }
      });

      if (state.selectedOption && state.selectedOption.name.toLowerCase() === filter.toLowerCase()) {
        return {
          ...state,
          filter: filter,
          filtredOptions: [],
        };
      }

      return {
        ...state,
        filter: filter,
        filtredOptions: filter.length < 1 ? [] : options,
        selectedOption: null
      };
    }

    case BreachActionTypes.OptionSelected: {
      return {
        ...state,
        selectedOption: action.payload
      };
    }

    case BreachActionTypes.ClearBreaches: {
      return {
        ...adapter.removeAll(state),
        filter: ''
      };
    }

    default: {
      return state;
    }
  }
}

export const reducers = {
  breaches: reducer,
  search: fromSearch.reducer
};
