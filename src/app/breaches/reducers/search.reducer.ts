import { BreachActions, BreachActionTypes } from '../actions/breach.actions';
import { Breach } from '../models/breach.model';

export interface State {
  result: Breach[];
  loading: boolean;
  error: string;
  query: string;
  domain: string;
}

const initialState: State = {
  result: [],
  loading: false,
  error: '',
  query: '',
  domain: ''
};

export function reducer(state = initialState, action: BreachActions): State {
  switch (action.type) {
    case BreachActionTypes.Search: {
      const query = action.payload;

      if (query === '') {
        return {
          ...state,
          loading: false,
          error: '',
          query,
        };
      }

      return {
        ...state,
        loading: true,
        error: '',
        query,
      };
    }

    case BreachActionTypes.SetSearchDomain: {
      const domain = action.payload;

      return {
        ...state,
        domain: domain
      };
    }

    case BreachActionTypes.SearchComplete: {
      return {
        ...state,
        result: action.payload,
        loading: false,
        error: '',
        query: state.query
      };
    }

    case BreachActionTypes.SearchError: {
      return {
        ...state,
        result: [],
        loading: false,
        error: action.payload,
      };
    }

    case BreachActionTypes.ClearSearchResults: {
      return {
        ...state,
        result: [],
        error: ''
      };
    }

    default: {
      return state;
    }
  }
}
