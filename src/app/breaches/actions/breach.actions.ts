import { Action } from '@ngrx/store';
import { Breach } from '../models/breach.model';
import { Option } from '../models/option.model';

export enum BreachActionTypes {
  LoadAllSuccess = '[Breach] LoadAll Success',
  LoadAll = '[Breach] LoadAll',
  LoadFail = '[Breach] LoadFail',
  Search = '[Breach] Search',
  SearchComplete = '[Breach] Search Complete',
  SearchError = '[Breach] Search Error',
  SetSearchDomain = '[Breach] Set Seearch Domain',
  ClearSearchResults = '[Breach] Clear Seearch Results',
  FilterBreaches = '[Breach] Filter Breaches',
  ClearBreaches = '[Breach] Clear Breaches',
  OptionSelected = '[Breach] Clear Autocomplete'
}

export class OptionSelected implements Action {
  readonly type = BreachActionTypes.OptionSelected;

  constructor(public payload: Option) {}
}

export class ClearBreaches implements Action {
  readonly type = BreachActionTypes.ClearBreaches;
}

export class FilterBreaches implements Action {
  readonly type = BreachActionTypes.FilterBreaches;

  constructor(public payload: string) {}
}

export class ClearSearchResults implements Action {
  readonly type = BreachActionTypes.ClearSearchResults;
}

export class Search implements Action {
  readonly type = BreachActionTypes.Search;

  constructor(public payload: string) {}
}

export class SetSearchDomain implements Action {
  readonly type = BreachActionTypes.SetSearchDomain;

  constructor(public payload: string) {}
}

export class SearchComplete implements Action {
  readonly type = BreachActionTypes.SearchComplete;

  constructor(public payload: Breach[]) {}
}

export class SearchError implements Action {
  readonly type = BreachActionTypes.SearchError;

  constructor(public payload: string) {}
}

export class LoadAllSuccess implements Action {
  readonly type = BreachActionTypes.LoadAllSuccess;

  constructor(public payload: Breach[]) {}
}

export class LoadAll implements Action {
  readonly type = BreachActionTypes.LoadAll;
}

export class LoadFail implements Action {
  readonly type = BreachActionTypes.LoadAll;

  constructor(public payload: string) {}
}

export type BreachActions =
  | OptionSelected
  | ClearBreaches
  | FilterBreaches
  | ClearSearchResults
  | LoadAllSuccess
  | LoadAll
  | LoadFail
  | Search
  | SearchComplete
  | SearchError
  | SetSearchDomain;
