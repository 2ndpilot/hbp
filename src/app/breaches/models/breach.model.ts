export interface Breach {
  Title: string;
  Name: string;
  Domain: string;
  Description: string;
  DataClasses: string[];
}
