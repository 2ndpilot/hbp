
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action, Store } from '@ngrx/store';
import { empty } from 'rxjs/observable/empty';
import * as fromBreaches from '../reducers/';
import * as fromSearch from '../reducers/search.reducer';
import * as fromRoot from '../../reducers/';

import { of } from 'rxjs/observable/of';
import {
  map,
  toArray,
  switchMap,
  catchError,
  debounceTime,
  skip,
  takeUntil,
  withLatestFrom
} from 'rxjs/operators';

import {
  BreachActionTypes,
  BreachActions,
  LoadAllSuccess,
  LoadAll,
  LoadFail,
  Search,
  SearchComplete,
  SearchError
} from '../actions/breach.actions';

import { Breach } from '../models/breach.model';
import { PwnedBreachService } from '../services/pwned-breach.service';

@Injectable()
export class BreachEffects {

  @Effect()
  loadBreaches$: Observable<Action> = this.actions$.pipe(
    ofType<LoadAll>(BreachActionTypes.LoadAll),
    switchMap(() =>
      this.breachService.getBreaches()
        .pipe(
          map((breaches: Breach[]) => new LoadAllSuccess(breaches)),
          catchError(error => of(new LoadFail(error)))
        )
    )
  );

  @Effect()
  searchBreaches$: Observable<Action> = this.actions$.pipe(
    ofType<Search>(BreachActionTypes.Search),
    debounceTime(300),
    map(action => action.payload),
    withLatestFrom(this.store$.select(fromBreaches.getSearchState)),
    switchMap(([query, searchState]) => {
      if (query === '') {
        return empty();
      }

      const nextSearch$ = this.actions$.pipe(
        ofType(BreachActionTypes.Search),
        skip(1)
      );

      return this.breachService
        .searchBreaches(query, searchState.domain)
        .pipe(
          takeUntil(nextSearch$),
          map((breach: Breach[]) => new SearchComplete(breach)),
          catchError(err => of(new SearchError(err.message)))
        );
    })
  );


  constructor(
    private actions$: Actions,
    private breachService: PwnedBreachService,
    private store$: Store<fromSearch.State>
  ) {}
}
