import { Component, ChangeDetectionStrategy, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'hbp-breach-search',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <mat-card>
    <mat-card-title>
      search
    </mat-card-title>
      <mat-card-content>
        <mat-input-container>
          <input matInput placeholder="Optional domain" (keyup)="setDomain.emit($event.target.value)">
        </mat-input-container>
        <mat-input-container>
          <input matInput placeholder="Search for an account" [value]="query" (keyup)="search.emit($event.target.value)">
        </mat-input-container>
        <mat-progress-bar [class.show]="searching" mode="buffer"></mat-progress-bar>
      </mat-card-content>
      <mat-card-footer><span *ngIf="error">{{error}}</span></mat-card-footer>
    </mat-card>
  `,
  styles: [
    `
    mat-card-footer {
      color: #FF0000;
      padding: 5px 0;
    }

    .mat-form-field {
      min-width: 300px;
    }
  `,
  ],
})
export class BreachSearchComponent {
  @Input() query = '';
  @Input() searching = false;
  @Input() error = '';
  @Output() search = new EventEmitter<string>();
  @Output() setDomain = new EventEmitter<string>();
}
