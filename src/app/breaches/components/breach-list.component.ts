import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { Breach } from '../models/breach.model';
import { filterMatch } from '../../shared/utils';

@Component({
  selector: 'hbp-breach-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <p *ngIf="error" class="loginError">
      {{ error }}
    </p>
    <mat-card *ngFor='let breach of breaches' [hidden]="filtered(breach, filter)">
      <mat-card-title-group>
        <img mat-card-avatar onError="this.src='assets/img/noun.png'" src="//logo.clearbit.com/{{breach.Domain}}?size=40">
        <mat-card-title>{{ breach.Title }}</mat-card-title>
      </mat-card-title-group>
      <mat-card-content>
        <p *ngIf="breach.Description" [innerHTML]="breach.Description"></p>
      </mat-card-content>
      <mat-card-footer>
        <mat-chip-list>
          <mat-basic-chip *ngFor="let chip of breach.DataClasses; let isLast=last">
            {{chip}}{{isLast ? '' : ', '}}
          </mat-basic-chip>
        </mat-chip-list>
      </mat-card-footer>
    </mat-card>
  `,
  styles: [
    `
    :host {
      display: flex;
      flex-wrap: wrap;
    }

    .mat-chip {
      margin-right: 5px;
    }

    :host a {
      display: flex;
    }

    .mat-card-footer {
      margin-top: 10px;
    }

    mat-card {
      width: 400px;
      margin: 15px;
      display: flex;
      flex-flow: column;
      justify-content: space-between;
    }

    .loginError {
      padding: 16px;
      color: white;
      background-color: red;
    }

    @media only screen and (max-width: 768px) {
      mat-card {
        margin: 15px 0 !important;
      }
    }
    mat-card:hover {
      box-shadow: 3px 3px 16px -2px rgba(0, 0, 0, .5);
    }
    mat-card-title {
      margin-right: 10px;
    }
    mat-card-title-group {
      margin: 0;
    }
    a {
      color: inherit;
      text-decoration: none;
    }
    mat-card-content {
      margin-top: 15px;
      margin: 15px 0 0;
    }
    span {
      display: inline-block;
      font-size: 13px;
    }
    mat-card-footer {
      padding: 0 25px 25px;
    }
  `]
})
export class BreachListComponent {
  @Input() breaches: Breach[];
  @Input() error: string;
  @Input() filter: string;

  filtered(breach, filter) {
    return !filterMatch(filter, breach);
  }
}
