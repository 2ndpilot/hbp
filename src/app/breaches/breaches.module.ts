import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewBreachListComponent } from './containers/view-breach-list.component';
import { SearchBreachAccountComponent } from './containers/search-breach-account.component';
import { BreachListComponent } from './components/breach-list.component';
import { BreachSearchComponent } from './components/breach-search.component';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { BreachEffects } from './effects/breach.effects';
import { MaterialModule } from '../material/material.module';
import { FiltredBreachesPipe } from './pipes/filtred-breaches.pipe';

import { reducers } from './reducers';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild([
      { path: '', component: ViewBreachListComponent },
      { path: 'search', component:  SearchBreachAccountComponent },
    ]),
    StoreModule.forFeature('breaches', reducers),
    EffectsModule.forFeature([BreachEffects])
  ],
  declarations: [
    ViewBreachListComponent,
    BreachListComponent,
    SearchBreachAccountComponent,
    BreachSearchComponent,
    FiltredBreachesPipe
  ]
})
export class BreachesModule { }
