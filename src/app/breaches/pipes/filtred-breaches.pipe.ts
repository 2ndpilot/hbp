import { Pipe, PipeTransform } from '@angular/core';

import { Breach } from '../models/breach.model';

@Pipe({ name: 'filtredBreaches' })
export class FiltredBreachesPipe implements PipeTransform {
  transform(breaches: Breach[], nameOrDomain) {
    return !nameOrDomain ? breaches : breaches.filter((breach) => {
      const titleMatch = breach.Title.toLowerCase().indexOf(nameOrDomain.toLowerCase()) !== -1;
      const domainMatch = breach.Domain.toLowerCase().indexOf(nameOrDomain.toLowerCase()) !== -1;
      return titleMatch || domainMatch;
    });
  }
}
