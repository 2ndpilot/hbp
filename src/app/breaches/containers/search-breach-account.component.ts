import { Component, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { take } from 'rxjs/operators';
import * as fromBreaches from '../reducers/';
import * as breach from '../actions/breach.actions';
import { Breach } from '../models/breach.model';


@Component({
  selector: 'hbp-search-breach-account',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <hbp-breach-search [searching]="loading$ | async" (setDomain)="setSearchDomain($event)" (search)="search($event)"></hbp-breach-search>
    <hbp-breach-list [error]="error$ | async" [breaches]="breaches$ | async"></hbp-breach-list>
  `
})
export class SearchBreachAccountComponent implements OnDestroy {

  breaches$: Observable<Breach[]>;
  loading$: Observable<boolean>;
  error$: Observable<string>;

  constructor(private store: Store<fromBreaches.State>) {
    this.breaches$ = store.pipe(select(fromBreaches.getSearchResults));
    this.loading$ = store.pipe(select(fromBreaches.getSearchLoading));
    this.error$ = store.pipe(select(fromBreaches.getSearchError));
  }

  setSearchDomain(domain: string) {
    this.store.dispatch(new breach.SetSearchDomain(domain));
  }

  search(query: string) {
    this.store.dispatch(new breach.Search(query));
  }

  ngOnDestroy() {
    this.store.dispatch(new breach.ClearSearchResults());
  }
}
