import { Component, ViewChild, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { withLatestFrom, map } from 'rxjs/operators';
import * as fromBreaches from '../reducers/';
import * as breach from '../actions/breach.actions';
import { Breach } from '../models/breach.model';
import { Option } from '../models/option.model';

@Component({
  selector: 'hbp-view-breaches',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <mat-card>
      <mat-card-title>
        list
      </mat-card-title>
      <mat-card-content>
        <mat-input-container>
          <input [matAutocomplete]="auto" matInput (keyup)="setFilter($event.target.value, $event)">
          <mat-autocomplete (optionSelected)="optionSelected($event)" autoActiveFirstOption #auto="matAutocomplete">
            <mat-option (click)="setFilter(option.name)" *ngFor="let option of filteredOptions$ | async" [value]="option.name">
              {{ option.name }}
            </mat-option>
          </mat-autocomplete>

        </mat-input-container>
        <mat-progress-bar [class.show]="loading$ | async" mode="buffer"></mat-progress-bar>
      </mat-card-content>
    </mat-card>
    <hbp-breach-list [filter]="filter$ | async" [breaches]="breaches$ | async"></hbp-breach-list>
  `
})
export class ViewBreachListComponent implements OnInit, OnDestroy {

  breaches$: Observable<Breach[]>;
  loading$: Observable<boolean>;
  filter$: Observable<string>;
  filteredOptions$: Observable<Option[]>;

  constructor(private store: Store<fromBreaches.State>) {
    this.breaches$ = store.pipe(select(fromBreaches.getAllBreaches));
    this.loading$ = store.pipe(select(fromBreaches.getBreachesLoading));
    this.filter$ = store.pipe(select(fromBreaches.getBreachesFilter));

    this.filteredOptions$ = store.pipe(select(fromBreaches.getAutocompleteOptions));
  }

  optionSelected($event) {
    this.store.dispatch(new breach.OptionSelected({name: $event.option.value}));
  }

  setFilter(filter: string) {
    this.store.dispatch(new breach.FilterBreaches(filter));
  }

  ngOnInit() {
    this.store.dispatch(new breach.LoadAll());
  }

  ngOnDestroy() {
    this.store.dispatch(new breach.ClearBreaches());
  }
}
