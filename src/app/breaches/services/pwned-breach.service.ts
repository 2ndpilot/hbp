import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { Breach } from '../models/breach.model';

@Injectable()
export class PwnedBreachService {
  private API_PATH = 'https://haveibeenpwned.com/api/v2';

  constructor(private http: HttpClient) {}

  getBreaches(): Observable<Breach[]> {
    return this.http.get<Breach[]>(`${this.API_PATH}/breaches/`);
  }

  searchBreaches(query, domain): Observable<Breach[]> {
    const url = `${this.API_PATH}/breachedaccount/${query}`;

    return this.http.get<Breach[]>(domain ? `${url}?domain=${domain}` : url);
  }
}
