import { RouterStateSerializer } from '@ngrx/router-store';
import { RouterStateSnapshot, Params } from '@angular/router';
import { Breach } from '../breaches/models/breach.model';

export function filterMatch(filter: string, breach: Breach) {
  if (filter) {
    const nameMatch = breach.Name.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
    const domainMatch = breach.Domain.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
    return nameMatch || domainMatch;
  } else {
    return true;
  }
}

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
}

export class CustomRouterStateSerializer
  implements RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    let route = routerState.root;

    while (route.firstChild) {
      route = route.firstChild;
    }

    const { url, root: { queryParams } } = routerState;
    const { params } = route;

    return { url, params, queryParams };
  }
}
