import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'hbp-root',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <mat-sidenav-container fullscreen>
      <hbp-toolbar></hbp-toolbar>
      <router-outlet></router-outlet>
    </mat-sidenav-container>
  `
})
export class AppComponent {}
