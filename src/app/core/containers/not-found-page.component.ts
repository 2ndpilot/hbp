import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'hbp-not-found-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `404`
})
export class NotFoundPageComponent {}
