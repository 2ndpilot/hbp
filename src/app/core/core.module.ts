import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AppComponent } from './containers/app.component';
import { NotFoundPageComponent } from './containers/not-found-page.component';
import { PwnedBreachService } from '../breaches/services/pwned-breach.service';
import { MaterialModule } from '../material/material.module';
import { ToolbarComponent } from './components/toolbar.component';
import { NavItemComponent } from './components/nav-item.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule
  ],
  declarations: [AppComponent, NotFoundPageComponent, ToolbarComponent, NavItemComponent],
  providers: [PwnedBreachService],
  exports: [AppComponent]
})
export class CoreModule { }
