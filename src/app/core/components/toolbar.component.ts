import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';

@Component({
  selector: 'hbp-toolbar',
  changeDetection : ChangeDetectionStrategy.OnPush,
  template: `
    <mat-toolbar color="primary" role="header">
      <hbp-nav-item routerLink="/">
        index
      </hbp-nav-item>
      <hbp-nav-item routerLink="/breaches/search">
        search
      </hbp-nav-item>
    </mat-toolbar>
  `
})
export class ToolbarComponent {}
