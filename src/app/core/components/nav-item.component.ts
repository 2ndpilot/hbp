import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'hbp-nav-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <button mat-button [routerLink]="routerLink">
      <ng-content></ng-content>
    </button>
  `,
  styles: [
    `
    .secondary {
      color: rgba(0, 0, 0, 0.54);
    }
  `,
  ],
})
export class NavItemComponent {
  @Input() routerLink: string | any[] = '/';
}
